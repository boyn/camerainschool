package top.boyn.camerainschool;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan(basePackages = "top.boyn.camerainschool.mapper")
@EnableTransactionManagement
@EnableScheduling
public class CamerainschoolApplication {
    public static void main(String[] args) {
        SpringApplication.run(CamerainschoolApplication.class, args);
    }
}
