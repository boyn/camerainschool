package top.boyn.camerainschool.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.camerainschool.service.CanteenService;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/11/7
 */
@RestController
public class CanteenController {
    @Resource
    CanteenService canteenService;
    @RequestMapping("/canteens")
    public Result getLatestCanteen(){
        return Result.OK().appendInfo("data", canteenService.getLatestCanteen());
    }
}
