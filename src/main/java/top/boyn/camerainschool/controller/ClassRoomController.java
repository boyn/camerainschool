package top.boyn.camerainschool.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.camerainschool.service.ClassRoomService;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/6
 * @description
 */
@RestController
public class ClassRoomController {
    @Resource
    ClassRoomService service;

    @RequestMapping("/classrooms/{buildingNumber}/{floor}")
    public Object getClassRooms(@PathVariable String buildingNumber,@PathVariable String floor) {
        return Result.OK().appendInfo("classrooms", service.getDistinctClassRoom(buildingNumber, floor));
    }

}
