package top.boyn.camerainschool.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Boyn
 * @date 2019/8/31
 * @description
 */
@Controller
public class PageController {
    @GetMapping("/__debug__")
    public String debugFilePage() {
        return "__debug__";
    }

    @GetMapping("/file")
    public String uploadFilePage() {

        return "file";
    }

    @GetMapping("/")
    public String indexPage(){
        return "index";
    }
}
