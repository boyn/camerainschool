package top.boyn.camerainschool.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.camerainschool.pojo.UploadFile;
import top.boyn.camerainschool.service.QiniuService;
import top.boyn.camerainschool.util.RandomNameGenerator;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Boyn
 * @date 2019/8/31
 * @description
 */
@RestController
@RequestMapping("/store")
public class QiniuController {
    @Resource
    QiniuService service;

    @GetMapping("/token")
    public Object token(){
        String fileName = RandomNameGenerator.generator();
        String token = service.upToken(fileName);
        return Result.OK().appendInfo("filename",fileName).appendInfo("token",token);
    }

    // 从七牛云中获取所有已经上传的文件
    @GetMapping("/allFiles")
    public Result getAllFiles(){
        List<UploadFile> uploadFiles = service.UserUploadFileList();
        return Result.OK().appendInfo("files", uploadFiles);
    }
}
