package top.boyn.camerainschool.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.camerainschool.pojo.ReId;
import top.boyn.camerainschool.service.ReIdService;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2020/3/26
 */
@RestController
public class ReIdController {
    @Resource
    ReIdService reIdService;
    @RequestMapping("/reid/{id}")
    public Result getReid(@PathVariable int id){
        if (id<0 || id>=3368 ){
            return Result.UserError().message("id只能支持在0~3367之间");
        }
        ReId reid = reIdService.getByQueryNum(id);
        return Result.OK().appendInfo("reid", reid);
    }
}
