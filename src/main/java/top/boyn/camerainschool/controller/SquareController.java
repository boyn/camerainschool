package top.boyn.camerainschool.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.camerainschool.pojo.Square;
import top.boyn.camerainschool.service.SquareService;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Boyn
 * @date 2020/3/20
 */
@RestController
public class SquareController {
    @Resource
    SquareService squareService;

    @RequestMapping("/square")
    public Result getSquare(){
        List<Square> squareList = squareService.getLatestSquare();
        return Result.OK().appendInfo("data", squareList);
    }
}
