package top.boyn.camerainschool.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.camerainschool.pojo.ClassRoom;
import top.boyn.camerainschool.service.ClassRoomService;
import top.boyn.camerainschool.service.UpdateService;
import top.boyn.camerainschool.util.DateFormatter;
import top.boyn.camerainschool.util.RandomNameGenerator;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Boyn
 * @date 2019/12/1
 */
@RestController
@Slf4j
public class UpdateVideoController {
    @Resource
    UpdateService updateService;
    @Resource
    ClassRoomService service;

    @RequestMapping("/update/{video_name}/{buildingNumber}/{floor}/{classRoomNumber}")
    public Object updateVideo(@PathVariable String video_name,
                              @PathVariable int buildingNumber,
                              @PathVariable int floor,
                              @PathVariable String classRoomNumber) {
        if (!updateService.divided_video(video_name)) {
            return Result.UserError().message("无此文件");
        }
        List<String> list = updateService.list_picture();
        int number = 0;
        for (String picture : list) {
            int detection = updateService.detection(picture);
            number += detection;
            log.info("人数:" + detection);
        }
        ClassRoom room = new ClassRoom();
        room.setBuildingNumber(buildingNumber);
        room.setFloor(floor);
        room.setRoomName(classRoomNumber);
        room.setNumber(number / list.size());
        room.setPhotoAddress(RandomNameGenerator.generator());
        room.setRoomDate(DateFormatter.formatDate(new Date()));

        service.insert(room);
        return Result.OK().message(String.valueOf(number / list.size()));
    }


}
