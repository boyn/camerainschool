package top.boyn.camerainschool.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import top.boyn.camerainschool.pojo.UploadFile;
import top.boyn.camerainschool.service.UploadFileService;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/18
 * @description
 */
@RestController
@RequestMapping("/store")
@Slf4j
public class UploadFileController {
    @Resource
    UploadFileService service;

    @PostMapping("/file")
    public Object storeFile(@RequestBody  UploadFile file) {
        service.storeFile(file);
        return Result.OK();
    }

    @GetMapping("/file")
    public Object list(@RequestParam("year")String year,@RequestParam("month")String month,@RequestParam("type")int type) {
        return Result.OK().appendInfo("list", service.getFiles(year, month,type));
    }
}
