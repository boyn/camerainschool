package top.boyn.camerainschool.controller;

import org.springframework.web.bind.annotation.*;
import top.boyn.camerainschool.pojo.User;
import top.boyn.camerainschool.service.UserService;
import top.boyn.camerainschool.util.Result;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author Boyn
 * @date 2019/8/31
 * @description
 */
@RestController
public class UserController {
    @Resource
    UserService service;

    @PostMapping("/login")
    public Result login(@RequestParam("number") String number,
                        @RequestParam("password") String password) {
        if (service.validate(number, password)) {
            User user = service.getUserByNumber(number);
            if(user==null) {
                return Result.OK().appendInfo("result", "登录失败").appendInfo("isLogin", false).appendInfo("message","用户不存在");
            }
            String username = user.getName();
            return Result.OK().appendInfo("result", "登录成功").appendInfo("isLogin", true).appendInfo("username", username).appendInfo("number", number);
        } else {
            return Result.OK().appendInfo("result", "登录失败").appendInfo("isLogin", false);
        }
    }

    @PostMapping("/register")
    public Result register(@RequestParam("number") String number,
                           @RequestParam("password") String password,@RequestParam("name")String name) {
        if(service.exists(number)) {
            return Result.OK().appendInfo("result", "用户已存在");
        }
        if(service.saveUser(name, number, password)){
            return Result.OK().appendInfo("result", "注册成功");
        }else{
            return Result.OK().appendInfo("result", "注册失败");
        }
    }



    @GetMapping("/user")
    public Object getUsersV2(){
        return Result.OK().appendInfo("users", service.getAllUsers());
    }
}
