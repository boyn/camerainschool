package top.boyn.camerainschool.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.boyn.camerainschool.pojo.Canteen;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/8/24
 * @description
 */

public interface CanteenMapper {
    List<Canteen> getAllCanteens();
    Canteen getCanteenByCanteenID(String canteenId);
    Canteen getCanteenById(int id);
    int insertCanteen(Canteen canteen);
    int deleteCanteen(int id);
}
