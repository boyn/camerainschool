package top.boyn.camerainschool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.boyn.camerainschool.pojo.ClassRoom;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/4
 * @description
 */
public interface ClassRoomMapper extends BaseMapper<ClassRoom> {
    void deleteBefore(String time);
    List<ClassRoom> getClassRoomsByBuildingAndFloor(int buildingNumber,int floor);
}
