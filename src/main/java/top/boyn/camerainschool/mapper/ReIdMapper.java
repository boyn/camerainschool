package top.boyn.camerainschool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.boyn.camerainschool.pojo.ReId;

/**
 * @author Boyn
 * @date 2020/3/26
 */
public interface ReIdMapper extends BaseMapper<ReId> {
}
