package top.boyn.camerainschool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.boyn.camerainschool.pojo.Square;

/**
 * @author Boyn
 * @date 2020/3/20
 */
public interface SquareMapper extends BaseMapper<Square> {
    Square getLatestSquareById(String squareId);
}
