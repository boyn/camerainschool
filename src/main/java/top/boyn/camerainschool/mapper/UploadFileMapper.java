package top.boyn.camerainschool.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import top.boyn.camerainschool.pojo.UploadFile;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/18
 * @description
 */
@TableName("upload_file")
public interface UploadFileMapper extends BaseMapper<UploadFile> {
    @Select("SELECT\n" +
            "    *\n" +
            "FROM\n" +
            "    upload_file \n" +
            "where  DATE_FORMAT(upload_time,'%Y%m') = #{yearAndMonth}")
    List<UploadFile> getByYearMonth(String yearAndMonth);
}
