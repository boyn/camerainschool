package top.boyn.camerainschool.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import top.boyn.camerainschool.pojo.User;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/1
 * @description
 */
public interface UserMapper extends BaseMapper<User> {
}
