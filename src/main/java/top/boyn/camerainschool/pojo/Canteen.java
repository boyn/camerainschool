package top.boyn.camerainschool.pojo;

import lombok.Data;
import top.boyn.camerainschool.pojo_enum.CanteenIdEnum;

import java.text.SimpleDateFormat;
import java.util.Date;

import static top.boyn.camerainschool.util.DateFormatter.formatDate;

/**
 * @author Boyn
 * @date 2019/8/23
 * @description 饭堂的映射类, 包含饭堂编号, 饭堂人数和插入时间
 */
@Data
public class Canteen {
    private int id;
    /**
     * 饭堂的编号
     */
    private CanteenIdEnum canteenId;
    /**
     * 饭堂人数
     */
    private int canteenNumber;
    /**
     * 检测时的日期
     */
    private String canteenDate;
    /**
     * 饭堂照片地址
     */
    private String photoAddress;


}
