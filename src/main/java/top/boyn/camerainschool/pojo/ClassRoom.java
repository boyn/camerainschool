package top.boyn.camerainschool.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


/**
 * @author Boyn
 * @date 2019/9/4
 * @description 教室
 */
@Data
@TableName("class_room")
public class ClassRoom {
    /**
     * ID
     */
    private int id;

    /**
     * 教学楼号,从1~12
     */
    private int buildingNumber;

    /**
     * 楼层
     */
    private int floor;

    /**
     * 教室号
     */
    private String roomName;

    /**
     * 教室人数
     */
    private int number;

    /**
     * 教室照片地址
     */
    private String photoAddress;

    /**
     * 检测时的日期
     */
    private String roomDate;


}
