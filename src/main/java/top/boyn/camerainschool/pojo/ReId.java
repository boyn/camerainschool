package top.boyn.camerainschool.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author Boyn
 * @date 2020/3/26
 */
@Data
@TableName("re_id")
public class ReId {
    @TableId
    private int queryNum;
    private String query;
    private String strong;
    private String weak;
}
