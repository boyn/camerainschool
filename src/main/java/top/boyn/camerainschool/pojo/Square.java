package top.boyn.camerainschool.pojo;

import lombok.Data;
import top.boyn.camerainschool.pojo_enum.CanteenIdEnum;

/**
 * 广场类
 * @author Boyn
 * @date 2020/3/20
 */
@Data
public class Square {
    private int id;
    /**
     * 广场的编号
     */
    private String squareId;
    /**
     * 广场人数
     */
    private int squareNumber;
    /**
     * 检测时的日期
     */
    private String squareDate;
    /**
     * 广场照片地址
     */
    private String photoAddress;
}
