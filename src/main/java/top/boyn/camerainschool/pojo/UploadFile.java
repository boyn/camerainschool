package top.boyn.camerainschool.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Boyn
 * @date 2019/9/1
 * @description 文件上传的信息
 */
@Data
public class UploadFile {
    /**
     * 文件名
     */
    @TableField("file_name")
    private String fileName;

    /**
     * 上传该文件的用户名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 上传该文件的用户Id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 上传日期
     */
    @TableField("upload_time")
    private String uploadTime;

    /**
     * 上传文件的类型
     */
    @TableField("type")
    private int type;
}
