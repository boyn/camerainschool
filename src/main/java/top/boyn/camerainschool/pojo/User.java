package top.boyn.camerainschool.pojo;

import lombok.Data;

/**
 * @author Boyn
 * @date 2019/9/1
 * @description 用户
 */
@Data
public class User {
    int id; // id
    String name; // 姓名
    String number; // 学号
    String password; // 密码
}
