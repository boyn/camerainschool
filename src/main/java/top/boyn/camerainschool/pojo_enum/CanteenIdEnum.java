package top.boyn.camerainschool.pojo_enum;

/**
 * @author Boyn
 * @date 2019/8/24
 * @description
 */
public enum CanteenIdEnum {
    一饭堂(1, "一饭堂"), 二饭堂(2, "二饭堂"), 三饭堂(3, "三饭堂"), 四饭堂(4, "四饭堂"), 五饭堂(5, "五饭堂"), UNKNOWN(6, "未知");

    private String canteenName;
    private int id;

    CanteenIdEnum(int id, String canteenName) {
        this.id = id;
        this.canteenName = canteenName;
    }

    private int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public String getCanteenName() {
        return canteenName;
    }

    private void setCanteenName(String canteenName) {
        this.canteenName = canteenName;
    }

    @Override
    public String toString() {
        return canteenName;
    }

    public static CanteenIdEnum getEnumById(int id) {
        for (CanteenIdEnum canteen : CanteenIdEnum.values()) {
            if (canteen.getId() == id) {
                return canteen;
            }
        }
        return UNKNOWN;
    }
}
