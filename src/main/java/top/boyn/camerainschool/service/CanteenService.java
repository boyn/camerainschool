package top.boyn.camerainschool.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import top.boyn.camerainschool.mapper.CanteenMapper;
import top.boyn.camerainschool.pojo.Canteen;
import top.boyn.camerainschool.pojo_enum.CanteenIdEnum;
import top.boyn.camerainschool.util.DateFormatter;
import javax.annotation.Resource;
import java.util.*;


/**
 * @author Boyn
 * @date 2019/8/23
 * @description 餐厅POJO的服务类
 */
@Service
public class CanteenService {
    @Resource
    private CanteenMapper canteenMapper;

    /**
     * 获得所有餐厅的列表\
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public List<Canteen> getAllCanteens(){
        return canteenMapper.getAllCanteens();
    }

    /**
     * 分别获取每个餐厅目前最新的一条信息
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public List<Canteen> getLatestCanteen(){
        List<Canteen> result = new ArrayList<>();
        result.add(canteenMapper.getCanteenByCanteenID("一饭堂"));
        result.add(canteenMapper.getCanteenByCanteenID("二饭堂"));
        result.add(canteenMapper.getCanteenByCanteenID("三饭堂"));
        result.add(canteenMapper.getCanteenByCanteenID("四饭堂"));
        result.add(canteenMapper.getCanteenByCanteenID("五饭堂"));
        return result;
    }
    /**
     * 将餐厅信息插入数据库中
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public int insertCanteen(int canteenId,int canteenNumber,String photoAddress){
        Canteen canteen = new Canteen();
        canteen.setCanteenDate(DateFormatter.formatDate(new Date()));
        canteen.setCanteenNumber(canteenNumber);
        canteen.setCanteenId(CanteenIdEnum.getEnumById(canteenId));
        canteen.setPhotoAddress(photoAddress);
        return canteenMapper.insertCanteen(canteen);
    }
}
