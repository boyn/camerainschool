package top.boyn.camerainschool.service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import top.boyn.camerainschool.mapper.ClassRoomMapper;
import top.boyn.camerainschool.pojo.ClassRoom;
import top.boyn.camerainschool.util.DateFormatter;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Boyn
 * @date 2019/9/4
 * @description
 */
@Service
public class ClassRoomService{
    @Resource
    private ClassRoomMapper mapper;

    /**
     * 删除三天前的数据,防止数据过多
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public void deleteData7DaysAgo(){
        String time = DateFormatter.formatDate(DateUtil.offsetDay(new DateTime(), -7));
        mapper.deleteBefore(time);
    }

    /**
     * 获取排序去重后的教室数据,根据时间排序,去重时会留下时间更近的
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public List<ClassRoom> getDistinctClassRoom(String buildingNumber, String floor){
        int buildingNum = 1 ;
        int floorNum = 1;
        try {
            buildingNum = Integer.parseInt(buildingNumber);
            floorNum = Integer.parseInt(floor);
        }catch (Exception e){
            return null;
        }
        if (buildingNum<1 || buildingNum > 12){
            return null;
        }
        if (floorNum < 1 || floorNum > 6) {
            return null;
        }
        return mapper.getClassRoomsByBuildingAndFloor(buildingNum,floorNum);
    }

    /**
     * 插入一条新记录
     */
    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public void insert(ClassRoom classRoom){
        mapper.insert(classRoom);
    }
}
