package top.boyn.camerainschool.service;


import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.BucketManager.FileListIterator;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import top.boyn.camerainschool.pojo.UploadFile;
import top.boyn.camerainschool.pojo.User;
import top.boyn.camerainschool.util.DateFormatter;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Boyn
 * @date 2019/8/31
 * @description 七牛云 对象存储服务的Service类
 */
@Service
@Slf4j
public class QiniuService {

    @Value("${qiniu.AccessKey}")
    String AccessKey;
    @Value("${qiniu.SecretKey}")
    String SecretKey;
    @Value("${qiniu.bucket}")
    String bucket;

    @Resource
    private UserService userService;

    private Auth auth = null;

    private String getAccessKey() {
        return AccessKey;
    }

    private String getSecretKey() {
        return SecretKey;
    }

    private void initAuth() {
        auth = Auth.create(AccessKey, SecretKey);
    }

    public String upToken(String fileName) {
        if (auth == null) {
            initAuth();
        }
        long expireSeconds = 3600;
        StringMap putPolicy = new StringMap();
        putPolicy.put("scope", "camera:" + fileName);
        return auth.uploadToken(bucket, null, expireSeconds, putPolicy);
    }

    /**
     * 列举每个用户上传的文件列表
     */
    public List<UploadFile> UserUploadFileList() {
        List<UploadFile> fileList = new ArrayList<>();
        Map<String, User> idNameMap = userService.getIdNameMap();
        FileListIterator fileListIterator = getFileListIterator();


        while (fileListIterator.hasNext()) {
            //处理获取的file list结果
            FileInfo[] items = fileListIterator.next();
            for (FileInfo item : items) {
                UploadFile file = new UploadFile();
                Date date = new Date(item.putTime / 10000);
                //文件名是以 **/***.**保存的  /号前面的作为插入的用户
                String fileName = item.key.split("/")[1];
                String userName = idNameMap.get(item.key.split("/")[0]).getName();
                file.setUserId(item.key.split("/")[0]);
                file.setFileName(fileName);
                file.setUserName(userName);
                file.setUploadTime(DateFormatter.formatDate(date));
                fileList.add(file);
            }
        }

        return fileList;
    }
    public String uploadFile(String filepath,String uploadName) {
        if (auth == null) {
            initAuth();
        }
        Configuration cfg = new Configuration(Region.region2());
//...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        String upToken = auth.uploadToken(bucket);
        try {
            Response response = uploadManager.put(filepath, uploadName, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            return putRet.hash;
        } catch (QiniuException ex) {
            Response r = ex.response;
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
        }
        return null;
    }

    /**
     * 列举每个用户上传的文件数量
     */
    @Deprecated
    public Map<String, Long> UserUploadFileCountByYearAndMonth(String year, String month) {
        List<UploadFile> fileList = UserUploadFileList();
        Map<String, Long> uploadFilesNumberMap = fileList.stream()
                .filter(x -> x.getUploadTime().split("-")[0].equals(year))
                .filter(x -> x.getUploadTime().split("-")[1].equals(month))
                .collect(Collectors.groupingBy(UploadFile::getUserName, Collectors.counting()));
        List<User> userList = userService.getAllUsers();

        //遍历一遍,如果没有上传文件记录的,要给他们插入数量0
        for (User user : userList) {
            if (!uploadFilesNumberMap.containsKey(user.getName())) {
                uploadFilesNumberMap.put(user.getName(), 0L);
            }
        }
        return uploadFilesNumberMap;
    }

    /**
     * 取得在七牛云中存储的文件列表迭代器
     */
    @Deprecated
    private FileListIterator getFileListIterator() {
        if (auth == null) {
            initAuth();
        }
        Configuration configuration = new Configuration(Region.autoRegion());
        BucketManager manager = new BucketManager(auth, configuration);
        //文件名前缀
        String prefix = "";
        //每次迭代的长度限制，最大1000，推荐值 1000
        int limit = 1000;
        //指定目录分隔符，列出所有公共前缀（模拟列出目录效果）。缺省值为空字符串
        String delimiter = "";
        return manager.createFileListIterator(bucket, prefix, limit, delimiter);
    }

}
