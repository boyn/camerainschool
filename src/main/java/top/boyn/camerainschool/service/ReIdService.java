package top.boyn.camerainschool.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.camerainschool.mapper.ReIdMapper;
import top.boyn.camerainschool.pojo.ReId;

import javax.annotation.Resource;
import java.io.File;
import java.util.UUID;

/**
 * @author Boyn
 * @date 2020/3/26
 */
@Service
@Slf4j
public class ReIdService {
    @Resource
    ReIdMapper reIdMapper;
    @Resource
    QiniuService service;

    public ReId getByQueryNum(int query) {
        QueryWrapper<ReId> wrapper = new QueryWrapper<>();
        wrapper.eq("query_num", query);
        ReId reId = reIdMapper.selectOne(wrapper);
        if (reId == null) {
            reId = getFromPythonAndUpload(query);
        }
        return reId;
    }

    public ReId getFromPythonAndUpload(int queryNum) {
        String path = getImagePath(queryNum);
        return uploadReId(queryNum, path);
    }

    public String getImagePath(int queryNum) {
        HttpRequest get = HttpUtil.createGet("localhost:5000/recognize/" + queryNum);
        HttpResponse execute = get.execute();
        JSONObject object = JSON.parseObject(execute.body());
        execute.close();
        String path = object.getString("result");
        return path;
    }

    public ReId uploadReId(int queryNum, String path) {
        ReId reId = new ReId();
        reId.setQueryNum(queryNum);
        String all = path + "\\outputs_all";
        File[] files = FileUtil.ls(all);
        String[] allFiles = new String[files.length - 1];
        for (int i = 0; i < files.length - 1; i++) {
            String pictureFilePath = all + "\\" + files[i].getName();
            String fName = UUID.randomUUID().toString();
            service.uploadFile(pictureFilePath, fName);
            allFiles[i] = fName;
        }
        String weak = path + "\\outputs_right";
        files = FileUtil.ls(weak);
        String[] rightFiles = new String[files.length - 1];
        for (int i = 0; i < files.length - 1; i++) {
            String pictureFilePath = weak + "\\" + files[i].getName();
            String fName = UUID.randomUUID().toString();
            service.uploadFile(pictureFilePath, fName);
            rightFiles[i] = fName;
        }
        String query = path + "\\outputs_all\\query.jpg";
        String fName = UUID.randomUUID().toString();
        service.uploadFile(query, fName);
        reId.setQuery(fName);
        reId.setStrong(StrUtil.join(",", allFiles));
        reId.setWeak(StrUtil.join(",", rightFiles));
        saveReId(reId);
        return reId;
    }

    public void saveReId(ReId reId) {
        reIdMapper.insert(reId);
    }
}
