package top.boyn.camerainschool.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import top.boyn.camerainschool.mapper.SquareMapper;
import top.boyn.camerainschool.pojo.Square;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Boyn
 * @date 2020/3/20
 */
@Service
@Slf4j
public class SquareService {
    @Resource
    SquareMapper squareMapper;

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public List<Square> getLatestSquare(){
        List<Square> list = new ArrayList<>();
        list.add(getLatestSquareByName("一食堂前广场"));
        list.add(getLatestSquareByName("东风广场"));
        list.add(getLatestSquareByName("图书馆前广场"));
        list.add(getLatestSquareByName("风雨操场"));
        list.add(getLatestSquareByName("体育馆前广场"));
        return list;
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public Square getLatestSquareByName(String squareName) {
        return squareMapper.getLatestSquareById(squareName);
    }

    @Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout = 4)
    public void insertSquare(Square square) {
        squareMapper.insert(square);
    }
}
