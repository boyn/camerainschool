package top.boyn.camerainschool.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import okhttp3.*;
import okhttp3.Request.Builder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Boyn
 * @date 2019/12/1
 */
@Service
public class UpdateService {
    OkHttpClient client = new OkHttpClient.Builder().build();
    public boolean divided_video(String video_name){
        Request request = new Request.Builder().url("http://localhost:5000/divide_frame/"+video_name).build();
        try (Response response = client.newCall(request).execute()) {
            JSONObject res = JSON.parseObject(response.body().string());
            if ("ERROR".equals(res.getString("status"))) {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public List<String> list_picture(){
        Request request = new Builder().url("http://localhost:5000/list").build();
        try (Response response = client.newCall(request).execute()) {
            JSONObject res = JSON.parseObject(response.body().string());
            JSONArray  array = res.getJSONArray("files");
            return array.toJavaList(String.class);
        } catch (IOException e) {
            // ... handle IO exception
        }
        return new ArrayList<>();
    }

    public int detection(String picture_name){
        Request request = new Builder().url("http://localhost:5000/detection/"+picture_name).build();
        try (Response response = client.newCall(request).execute()) {
            JSONObject res = JSON.parseObject(response.body().string());
            return res.getIntValue("number");
        } catch (IOException e) {
            // ... handle IO exception
        }
        return 0;
    }

}
