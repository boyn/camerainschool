package top.boyn.camerainschool.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.camerainschool.mapper.UploadFileMapper;
import top.boyn.camerainschool.pojo.UploadFile;
import top.boyn.camerainschool.pojo.User;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Boyn
 * @date 2019/9/18
 * @description
 */
@Service
@Slf4j
public class UploadFileService {
    @Resource
    UploadFileMapper mapper;
    @Resource
    UserService service;

    private Map<String, User> idNameMap = null;

    public void storeFile(UploadFile file) {
        if (idNameMap == null) {
            idNameMap = service.getIdNameMap();
        }
        if (file.getUserName() == null) {
            file.setUserName(idNameMap.get(String.valueOf(file.getUserId())).getName());
        }
        log.info(file.toString());
        mapper.insert(file);
    }

    public List<UploadFile> getFilesByType(int type){
        QueryWrapper<UploadFile> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", type);
        return mapper.selectList(queryWrapper);
    }

    public Map<String, Long> getFiles(String year, String month, int type) {
        if (idNameMap == null) {
            idNameMap = service.getIdNameMap();
        }
        List<UploadFile> fileList = mapper.getByYearMonth(year + month);
        Map<String, Long> uploadFilesNumberMap = fileList.stream()
                .filter(x -> x.getType() == type)
                .collect(Collectors.groupingBy(UploadFile::getUserName, Collectors.counting()));
        for (User user : idNameMap.values()) {
            if (!uploadFilesNumberMap.containsKey(user.getName())) {
                uploadFilesNumberMap.put(user.getName(), 0L);
            }
        }

        return uploadFilesNumberMap;

    }
}
