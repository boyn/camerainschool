package top.boyn.camerainschool.service;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import top.boyn.camerainschool.mapper.UserMapper;
import top.boyn.camerainschool.pojo.User;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/1
 * @description
 */
@Service
public class UserService {
    @Resource
    private UserMapper mapper;

    /**
     * User列表相对不发生变化,将其缓存下来,经过测试读取速度有300倍提升
     */
    private List<User> userList = null;

    private Map<String, User> idNameMap = null;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 4)
    public boolean saveUser(String username, String number, String password) {
        User user = new User();
        if (
                username.length() < 3 ||
                !StringUtils.isNumber(number) ||
                number.length() < 8 ||
                password.length() < 6) {
            return false;
        }
        user.setName(username);
        user.setNumber(number);
        user.setPassword(password);
        mapper.insert(user);
        return true;
    }

    public boolean exists(String number) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("number", number);
        return mapper.selectList(wrapper).size() != 0;
    }

    /**
     * 获取所有用户
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 4)
    public List<User> getAllUsers() {
        if (userList == null) {
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            userList = mapper.selectList(wrapper);
            return userList;
        } else {
            return userList;
        }
    }

    /**
     * 验证用户传入的密码是否与存入的密码相同
     *
     * @param number
     * @param password
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 4)
    public boolean validate(String number, String password) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("number", number);
        User user = mapper.selectOne(wrapper);
        return user != null && user.getPassword().equals(password);
    }

    /**
     * 获取用户Id与名字对应的索引表
     */
    public Map<String, User> getIdNameMap() {
        if (idNameMap == null) {
            idNameMap = new HashMap<String, User>();
            if (userList == null) {
                getAllUsers();
            }
            for (User user : userList) {
                idNameMap.put(String.valueOf(user.getNumber()), user);
            }
        }
        return idNameMap;
    }

    /**
     * 根据学号获取对应User
     */
    public User getUserByNumber(String number) {
        if (idNameMap == null) {
            getIdNameMap();
        }
        User user = idNameMap.get(number);
        if( user == null) {
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("number", number);
            user = mapper.selectOne(wrapper);
        }
        return user;
    }
}
