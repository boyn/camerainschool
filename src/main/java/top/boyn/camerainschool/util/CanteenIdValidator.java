package top.boyn.camerainschool.util;

import java.util.Arrays;
import java.util.List;

/**
 * @author Boyn
 * @date 2019/8/24
 * @description 验证餐厅的名字是否为
 * ONE,TWO,THREE,FOUR,FIVE
 */
public class CanteenIdValidator {
    private static List<String> names = Arrays.asList("ONE", "TWO", "THREE", "FOUR", "FIVE");
    public static boolean valid(String name) {
        if (name != null) {
            return names.contains(name);
        }
        return false;
    }
}
