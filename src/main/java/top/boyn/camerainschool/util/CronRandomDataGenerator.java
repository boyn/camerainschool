package top.boyn.camerainschool.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import top.boyn.camerainschool.pojo.ClassRoom;
import top.boyn.camerainschool.pojo.Square;
import top.boyn.camerainschool.pojo.UploadFile;
import top.boyn.camerainschool.service.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * 定时产生一些随机数据
 *
 * @author Boyn
 * @date 2020/3/20
 */
@Component
@Slf4j
public class CronRandomDataGenerator {
    @Resource
    UploadFileService service;
    @Resource
    CanteenService canteenService;
    @Resource
    ClassRoomService classRoomService;
    @Resource
    SquareService squareService;

    // 将教室的过期数据进行淘汰
    public void Delete7DaysAgo(){
        classRoomService.deleteData7DaysAgo();
    }

    // 生成饭堂的数据
    public void GenerateCanteenData(List<UploadFile> photoList) {
        Random r = new Random(new Date().getTime());
        for (int i = 1; i <= 5; i++) {
            UploadFile file = photoList.get(r.nextInt(photoList.size()) % photoList.size());
            canteenService.insertCanteen(i, Integer.parseInt(file.getUserId()), file.getFileName() + ";" + file.getUserName());
        }
    }

    // 生成广场的数据
    public void GenerateSquareData(List<UploadFile> photoList) {
        Random r = new Random(new Date().getTime());
        String[] list = new String[]{"一食堂前广场", "东风广场", "图书馆前广场", "风雨操场", "体育馆前广场"};
        for (String s : list) {
            UploadFile file = photoList.get(r.nextInt(photoList.size()) % photoList.size());
            Square square = new Square();
            square.setPhotoAddress(file.getFileName() + ";" + file.getUserName());
            square.setSquareDate(DateFormatter.formatDate(new Date()));
            square.setSquareId(s);
            square.setSquareNumber(Integer.parseInt(file.getUserId()));
            squareService.insertSquare(square);
        }
    }

    // 生成教室的数据
    public void GenerateClassroomData(List<UploadFile> photoList) {
        Random r = new Random(new Date().getTime());
        for (int i = 1; i <= 12; i++) {
            for (int j = 1; j <= 6; j++) {
                for (int k = 1; k <= 8; k++) {
                    UploadFile file = photoList.get(r.nextInt(photoList.size()) % photoList.size());
                    ClassRoom classRoom = new ClassRoom();
                    classRoom.setBuildingNumber(i);
                    classRoom.setFloor(j);
                    classRoom.setRoomName(j + "0" + k);
                    classRoom.setRoomDate(DateFormatter.formatDate(new Date()));
                    classRoom.setNumber(Integer.parseInt(file.getUserId()));
                    classRoom.setPhotoAddress(file.getFileName() + ";" + file.getUserName());
                    classRoomService.insert(classRoom);
                }
            }
        }
    }

    @Scheduled(fixedDelay = 1800000)
    public void GenerateData() {
        List<UploadFile> uploadFiles = service.getFilesByType(1);
        List<UploadFile> photoList = addWebsite(uploadFiles);
        GenerateCanteenData(photoList);
        GenerateSquareData(photoList);

        List<UploadFile> uploadFilesType2 = service.getFilesByType(2);
        List<UploadFile> photoListType2 = addWebsite(uploadFilesType2);
        GenerateClassroomData(photoListType2);

        Delete7DaysAgo();
    }

    public List<UploadFile> addWebsite(List<UploadFile> uploadFiles) {
        return uploadFiles.stream().peek(f -> {
            String[] filenames = f.getFileName().split(";");
            filenames[0] = "http://camera.boyn.top/" + filenames[0];
            filenames[1] = "http://camera.boyn.top/" + filenames[1];
            f.setFileName(filenames[0] + ";" + filenames[1]);

            String[] usernames = f.getUserName().split(";");
            usernames[0] = "http://camera.boyn.top/" + usernames[0];
            usernames[1] = "http://camera.boyn.top/" + usernames[1];
            f.setUserName(usernames[0] + ";" + usernames[1]);
        }).collect(Collectors.toList());
    }
}
