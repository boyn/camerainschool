package top.boyn.camerainschool.util;

import lombok.extern.slf4j.Slf4j;

import javax.xml.crypto.Data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Boyn
 * @date 2019/8/24
 * @description 日期格式化的工具类
 */
@Slf4j
public class DateFormatter {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static String formatDate(Date date) {
        synchronized (sdf) {
            return sdf.format(date);
        }
    }
    public static Date formatDate(String dateString)  {
        synchronized (sdf) {
            try {
                return sdf.parse(dateString);
            }catch (ParseException e){
                log.error("日期格式错误");
            }

        }
        return null;
    }
}
