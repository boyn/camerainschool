package top.boyn.camerainschool.util;

import java.util.UUID;

/**
 * @author Boyn
 * @date 2019/8/24
 * @description 随机文件名字的生成器
 */
public class RandomNameGenerator {
    public static String generator(){
        return UUID.randomUUID().toString();
    }
}
