package top.boyn.camerainschool.util;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description 统一请求构建类
 */
@Data
public class Result {
    private int statusCode;
    private String message;
    private Map<String, Object> resultInfo;

    private Result(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
        this.resultInfo = new HashMap<>();
    }

    public static Result OK() {
        return new Result(200, "success");
    }

    public static Result UserError() {
        return new Result(400, "UserError");
    }

    public static Result ServerError() {
        return new Result(500, "ServerError");
    }

    public static Result CodeAndMessage(@NotNull int code, @NotNull String message) {
        return new Result(code, message);
    }

    public Result code(@NotNull int code) {
        this.statusCode = code;
        return this;
    }

    public Result message(@NotNull String message) {
        this.message = message;
        return this;
    }

    public Result appendInfo(@NotNull String key, @NotNull Object value) {
        this.resultInfo.put(key, value);
        return this;
    }

}
