package top.boyn.camerainschool.util;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/8/31
 * @description 自定义返回体
 */
@Deprecated
public class ResultBuilder {
    private Result result;

    public ResultBuilder() {
        result = new Result();
        result.resultInfo = new HashMap<>();
    }

    public ResultBuilder OK() {
        result.setStatusCode(200);
        result.setMessage("OK");
        return this;
    }

    public ResultBuilder Error() {
        result.setStatusCode(500);
        result.setMessage("Error");
        return this;
    }

    public ResultBuilder returnStatus(@NotNull int statusCode, @NotNull String message) {
        result.setMessage(message);
        result.setStatusCode(statusCode);
        return this;
    }

    public ResultBuilder appendInfo(@NotNull String key, @NotNull Object value) {
        result.getResultInfo().put(key, value);
        return this;
    }

    public Result build() {
        return this.result;
    }

    class Result {
        public int statusCode;
        public String message;
        public Map<String, Object> resultInfo;

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Map<String, Object> getResultInfo() {
            return resultInfo;
        }

        public void setResultInfo(Map<String, Object> resultInfo) {
            this.resultInfo = resultInfo;
        }
    }
}
