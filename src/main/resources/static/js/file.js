var NameIdMap = {};
$(function () {
    axios.get("http://120.78.1.10:8080/user")
        .then(function (response) {
            users = response.data.resultInfo.users;
            for (let i = 0; i < users.length; i++) {
                NameIdMap[users[i].name] = users[i].id;
                button = $("<button class='dropdown-item'></button>").text(users[i].name);
                button.click(function () {
                    $("#chooseUser").text($(this)[0].innerHTML)
                });
                $("#users").append(button)
            }
        });
    const myDate = new Date();
    const year = myDate.getFullYear();
    let month = myDate.getMonth() + 1;
    if (month < 10) {
        month = "0" + month
    }
    axios.get("http://120.78.1.10:8080/store/file?year=" + year + "&month=" + month + "&type=" + "1")
        .then(function (response) {
            let list = response.data.resultInfo.list;
            for (key in list) {
                $("#photo_progress").append($("<div></div>").text(key + ":" + list[key]))
            }
        });
    axios.get("http://120.78.1.10:8080/store/file?year=" + year + "&month=" + month + "&type=" + "2")
        .then(function (response) {
            let list = response.data.resultInfo.list;
            for (key in list) {
                $("#vedio_progress").append($("<div></div>").text(key + ":" + list[key]))
            }
        });
    $("#file1").change(function () {
        $("#PhotoFileDisplay").empty();
        var files = $("#file1")[0].files;
        if (files.length != 0) {
        }
        for (let i = 0; i < files.length; i++) {
            $("#PhotoFileDisplay").append($("<div class='text-left'></div>").text(files[i].name))
        }
    });
    $("#file2").change(function () {
        $("#VideoFileDisplay").empty();
        var files = $("#file2")[0].files;
        if (files.length != 0) {
        }
        for (let i = 0; i < files.length; i++) {
            $("#VideoFileDisplay").append($("<div class='text-left'></div>").text(files[i].name))
        }
    })
});

function chooseFile(type) {
    $("#file" + type).click()
}

function uploadFile(type) {
    if (type === 2) {
        uploadVideo();
        return;
    }
    var token;
    var files = $("#file" + type)[0].files;
    var counter = files.length;
    if (counter === 0) {
        alert("请添加文件");
        return;
    }
    for (let i = 0; i < files.length; i++) {
        axios.get("http://120.78.1.10:8080/store/token")
            .then(function (response) {
                if (response.data.statusCode == "200") {
                    data = response.data.resultInfo;
                    token = data.token;
                    filename = data.filename;
                    var Username = $("#chooseUser").text();
                    if (Username == "选择") {
                        alert("请选择用户名");
                        return;
                    }
                    var UserId = NameIdMap[$("#chooseUser").text()];
                    var form = new FormData();
                    file = files[i];
                    var index = file.name.lastIndexOf(".");
                    fileExpend = file.name.substring(index + 1, file.name.length);
                    form.append("key", UserId + "/" + filename + "." + fileExpend);
                    form.append("token", token);
                    form.append("file", file);

                    axios({
                        method: 'post',
                        url: 'http://upload-z2.qiniup.com',
                        data: form
                    }).then(function (response) {
                        code = response["status"];
                        var uploadTime;
                        if (code == 200) {
                            var form = new FormData();
                            userId = response["data"]["key"].split("/")[0];
                            fileName = response["data"]["key"].split("/")[1];
                            today = new Date();
                            let month = today.getMonth() + 1;
                            if (month < 10) {
                                month = "0" + month
                            }
                            uploadTime = today.getFullYear() + "-" + month + "-" + today.getDay();
                            form.append("fileName", fileName);
                            form.append("userId", userId);
                            form.append("uploadTime", uploadTime);
                            form.append("type", type.toString());

                            axios({
                                method: 'post',
                                url: 'http://120.78.1.10:8080/store/file',
                                data: form
                            });
                            /**
                             * 这里的counter初始值是文件的数量,当上传文件成功一个时
                             * 会减少counter计数,当文件减少到1时,说明已经全部上传成功
                             */
                            if (counter == 1) {
                                alert("上传成功");
                                location.reload()
                            } else {
                                counter--;
                            }
                        }

                    });
                }
            })
    }
}

function uploadVideo() {
    if($("#file2")[0].files.length==0){
        alert("请添加文件");
        return;
    }
    $(".progress").css("display","");
    var video = $("#file2")[0].files[0];
    axios.get("http://120.78.1.10:8080/store/token")
        .then(function (response) {
            if (response.data.statusCode == "200") {
                data = response.data.resultInfo;
                token = data.token;
                filename = data.filename;
                var config = {
                    useCdnDomain: true,
                    region: qiniu.region.z2
                };
                var observer = {
                    next(res) {
                        changeProgress(parseInt(res["total"]["percent"]));
                    },
                    error(err) {

                    },
                    complete(res) {
                        changeProgress(100);
                        var form = new FormData();
                        userId = res["key"].split("/")[0];
                        fileName = res["key"].split("/")[1];
                        today = new Date();
                        let month = today.getMonth() + 1;
                        if (month < 10) {
                            month = "0" + month
                        }
                        uploadTime = today.getFullYear() + "-" + month + "-" + today.getDay();
                        form.append("fileName", fileName);
                        form.append("userId", userId);
                        form.append("uploadTime", uploadTime);
                        form.append("type", "2");

                        axios({
                            method: 'post',
                            url: 'http://120.78.1.10:8080/store/file',
                            data: form
                        }).then(function (response) {
                            alert("上传成功");
                            location.reload()
                        })
                    }
                };
                var Username = $("#chooseUser").text();
                if (Username == "选择") {
                    alert("请选择用户名");
                    return;
                }
                let UserId = NameIdMap[$("#chooseUser").text()];
                let index = video.name.lastIndexOf(".");
                let fileExpend = video.name.substring(index + 1, video.name.length);
                let filename = UserId + "/" + filename + "." + fileExpend;
                let putExtra = {
                    fname: filename
                };
                let observable = qiniu.upload(video, filename, token, putExtra, config);
                observable.subscribe(observer)

            }
        });
}

function changeProgress(progress) {
    $(".progress-bar").css("width",progress+"%");
}