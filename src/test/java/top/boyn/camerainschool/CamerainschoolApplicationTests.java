package top.boyn.camerainschool;

import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class CamerainschoolApplicationTests {

    @Resource
    DataSource dataSource;

    @Test
    public void haveDataSource(){
        assertNotNull(dataSource);
    }

    @Test
    public void contextLoads() {
    }

}
