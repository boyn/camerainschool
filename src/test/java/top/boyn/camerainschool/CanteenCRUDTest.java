package top.boyn.camerainschool;

import cn.hutool.json.JSONException;
import cn.hutool.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.camerainschool.mapper.CanteenMapper;
import top.boyn.camerainschool.pojo.Canteen;
import top.boyn.camerainschool.pojo.ClassRoom;
import top.boyn.camerainschool.pojo_enum.CanteenIdEnum;
import top.boyn.camerainschool.service.CanteenService;
import top.boyn.camerainschool.util.DateFormatter;
import top.boyn.camerainschool.util.RandomNameGenerator;
import top.boyn.camerainschool.util.StdRandom;

import javax.annotation.Resource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author Boyn
 * @date 2019/8/23
 * @description Canteen的数据库CRUD测试
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class CanteenCRUDTest {
    @Resource
    CanteenMapper mapper;

    @Resource
    CanteenService service;

    @Test
    public void getCanteen(){
        List<Canteen> canteens = service.getAllCanteens();
        System.out.println(canteens);
    }

    @Test
    public void getCanteenById(){
        Canteen canteen = mapper.getCanteenById(2);
        assertEquals(canteen.getCanteenId(),CanteenIdEnum.四饭堂);
    }

    @Test
    public void getLatestCanteen(){
        List<Canteen> allCanteens = service.getLatestCanteen();
        for (Canteen c : allCanteens) {
            System.out.println(c);
        }
    }

    @Test
    public void insertCanteen(){
        for (int i = 0; i < 1000; i++) {
            Canteen canteen = new Canteen();
            canteen.setPhotoAddress(RandomNameGenerator.generator());
            canteen.setCanteenNumber(StdRandom.randomInt(100));
            canteen.setCanteenDate(DateFormatter.formatDate(StdRandom.randomDate()));
            canteen.setCanteenId(CanteenIdEnum.getEnumById(StdRandom.randomInt(1,6)));
            mapper.insertCanteen(canteen);
        }
    }

    @Test
    public void insertAndDeleteCanteen(){
        Canteen canteen = new Canteen();
        canteen.setCanteenDate(DateFormatter.formatDate(new Date()));
        canteen.setCanteenNumber(33);
        canteen.setCanteenId(CanteenIdEnum.一饭堂);
        canteen.setPhotoAddress(RandomNameGenerator.generator()+".png");
        int number = mapper.insertCanteen(canteen);
        assertEquals(number,1L);
        int id = canteen.getId();
        number = mapper.deleteCanteen(id);
        assertEquals(number,1L);
    }




    /*@Test
    public void addCanteen() {
        Canteen canteen = new Canteen("FOUR", 13);
        assertEquals(service.(canteen),Integer.valueOf(1));
    }*/
}
