package top.boyn.camerainschool;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.camerainschool.mapper.ClassRoomMapper;
import top.boyn.camerainschool.pojo.ClassRoom;
import top.boyn.camerainschool.service.ClassRoomService;
import top.boyn.camerainschool.util.DateFormatter;
import top.boyn.camerainschool.util.RandomNameGenerator;
import top.boyn.camerainschool.util.StdRandom;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Boyn
 * @date 2019/9/4
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class ClassRoomCRUDTest {
    @Resource
    private ClassRoomMapper mapper;

    @Resource
    private ClassRoomService classRoomService;

    @Test
    public void testInsertClassRoom(){
        for (int i = 0; i < 1000; i++) {
            int floor = StdRandom.randomInt(1,6);
            int room = StdRandom.randomInt(1,10);
            ClassRoom classRoom = new ClassRoom();
            classRoom.setBuildingNumber(StdRandom.randomInt(1,13));
            classRoom.setFloor(floor);
            classRoom.setNumber(StdRandom.randomInt(50));
            classRoom.setRoomDate(DateFormatter.formatDate(StdRandom.randomDate()));
            classRoom.setRoomName(floor+"0"+room);
            classRoom.setPhotoAddress(RandomNameGenerator.generator());
            mapper.insert(classRoom);
        }

    }

    @Test
    public void testClassRoom(){
        ClassRoom classRoom = new ClassRoom();
        classRoom.setBuildingNumber(1);
        classRoom.setFloor(2);
        classRoom.setNumber(14);
        classRoom.setRoomDate(DateFormatter.formatDate(new Date()));
        classRoom.setRoomName("905");
        classRoom.setPhotoAddress(RandomNameGenerator.generator());
        int number = mapper.insert(classRoom);
        assertEquals(number,1L);
    }

    @Test
    public void testGetList(){
        System.out.println(classRoomService.getDistinctClassRoom("1","1"));
    }
}
