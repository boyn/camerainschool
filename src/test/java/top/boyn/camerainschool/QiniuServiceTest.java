package top.boyn.camerainschool;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.camerainschool.pojo.ReId;
import top.boyn.camerainschool.pojo.UploadFile;
import top.boyn.camerainschool.service.QiniuService;
import top.boyn.camerainschool.service.ReIdService;
import top.boyn.camerainschool.service.UploadFileService;

import javax.annotation.Resource;
import java.io.File;
import java.util.UUID;

/**
 * @author Boyn
 * @date 2019/8/31
 * @description 七牛云存储的测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class QiniuServiceTest {
    @Resource
    QiniuService service;
    @Resource
    UploadFileService uploadFileService;
    @Resource
    ReIdService reIdService;

    @Test
    public void test() {
        System.out.println(service.upToken("12.jpg"));
    }

    @Test
    public void getUserFileList() {
        System.out.println(service.UserUploadFileList());
    }

    @Test
    public void getUserFileCount() {
        System.out.println(service.UserUploadFileCountByYearAndMonth("2019", "09"));
    }

    public void uploadReId(int id){
        ReId reId = new ReId();
        reId.setQueryNum(id);
        String all = "F:\\Code\\python\\大创\\Re_ID\\outputs_all";
        File[] files = FileUtil.ls(all);
        String[] allFiles = new String[files.length-1];
        for (int i = 0;i<files.length-1;i++) {
            String pictureFilePath = all + "\\" + files[i].getName();
            String fName = UUID.randomUUID().toString();
            service.uploadFile(pictureFilePath, fName);
            allFiles[i] = fName;
        }
        String weak = "F:\\Code\\python\\大创\\Re_ID\\outputs_right";
        files = FileUtil.ls(weak);
        String[] rightFiles = new String[files.length-1];
        for(int i = 0;i<files.length-1;i++){
            String pictureFilePath = weak + "\\" + files[i].getName();
            String fName = UUID.randomUUID().toString();
            service.uploadFile(pictureFilePath,fName);
            rightFiles[i] = fName;
        }
        String query = "F:\\Code\\python\\大创\\Re_ID\\outputs_all\\query.jpg";
        String fName = UUID.randomUUID().toString();
        service.uploadFile(query, fName);
        reId.setQuery(fName);
        reId.setStrong(StrUtil.join(",",allFiles));
        reId.setWeak(StrUtil.join(",",rightFiles));
        reIdService.saveReId(reId);
        System.out.println(reId);
    }

    @Test
    public void upload() {
        String output = "F:\\Code\\python\\大创\\Head_Detection\\output";
        String picture = "F:\\Code\\python\\大创\\Head_Detection\\picture";
        String appPicture = "F:\\Code\\python\\大创\\Head_Detection\\APP_picture";
        String appOutput = "F:\\Code\\python\\大创\\Head_Detection\\APP_output";

        File[] files = FileUtil.ls(output);

        for (File f : files) {
            String pictureFilename = UUID.randomUUID().toString();
            String outputFilename = UUID.randomUUID().toString();
            String appPictureFilename = UUID.randomUUID().toString();
            String appOutputFilename = UUID.randomUUID().toString();

            UploadFile uploadFile = new UploadFile();
            String pictureFilePath = picture + "\\" + f.getName().split("_")[0] + ".jpg";
            String appPictureFilePath = appPicture + "\\" + f.getName().split("_")[0] + ".jpg";
            String outputFilePath = output + "\\" + f.getName();
            String appOutputFilePath = appOutput + "\\" + f.getName();

            String number = f.getName().split("_")[1].split("\\.")[0];

            service.uploadFile(pictureFilePath, pictureFilename);
            service.uploadFile(appPictureFilePath, appPictureFilename);
            service.uploadFile(outputFilePath, outputFilename);
            service.uploadFile(appOutputFilePath, appOutputFilename);

            uploadFile.setFileName(pictureFilename + ";" + appPictureFilename);
            uploadFile.setUserName(outputFilename + ";" + appOutputFilename);
            uploadFile.setUploadTime(DateUtil.now());
            uploadFile.setUserId(number);
            uploadFile.setType(2);
            uploadFileService.storeFile(uploadFile);
        }
    }

}
