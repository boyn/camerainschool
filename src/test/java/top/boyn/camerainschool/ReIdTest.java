package top.boyn.camerainschool;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.util.Strings;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

/**
 * @author Boyn
 * @date 2020/3/30
 */
@Ignore
public class ReIdTest {
    @Test
    public void TestConnect() {
        HttpRequest get = HttpUtil.createGet("localhost:5000/recognize/2");
        HttpResponse execute = get.execute();
        JSONObject object = JSON.parseObject(execute.body());
        execute.close();
        String path = object.getString("result");
        String outputAll = path+"\\outputs_all";
        File[] ls = FileUtil.ls(outputAll);
        for(File f : ls) {
            System.out.println(f.getName());
        }
    }
}
