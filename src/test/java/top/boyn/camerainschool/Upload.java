package top.boyn.camerainschool;

import org.junit.Ignore;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * @author Boyn
 * @date 2020/3/21
 */
@Ignore
public class Upload {


    @Test
    public void upload() throws Exception {
        String CONTENT_TYPE = "multipart/form-data"; //内容类型
        String BOUNDARY = "FlPm4LpSXsE" ; //定义数据分隔符(可以随意更改)
        URL u;
        HttpURLConnection connection = null;
        try {
            u = new URL("http://localhost:8080/login");
            connection = (HttpURLConnection) u.openConnection();
            connection.setRequestMethod("POST");//"POST" "GET"
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            connection.setInstanceFollowRedirects(true);

            connection.setRequestProperty("Content-Type", CONTENT_TYPE + ";boundary=" + BOUNDARY);
//                connection.setRequestProperty("Content-Type", "application/json");
//                connection.setRequestProperty("Content-Type", "multipart/form-data");
            connection.setRequestProperty("Accept", "*/*");
            connection.setConnectTimeout(10000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        DataOutputStream ds = new DataOutputStream(connection.getOutputStream());
        String lineStart = "--";
        String boundary = "FlPm4LpSXsE";    // 数据分隔符
        String lineEnd ="\r\n";

        StringBuilder sb = new StringBuilder();
        sb.append(lineStart  + boundary + lineEnd);
        sb.append("Content-Disposition: form-data; name=\"number\"" + lineEnd);
        sb.append(lineEnd);
        sb.append("2017212036"+ lineEnd);
        sb.append(lineStart + boundary + lineEnd);
        sb.append("Content-Disposition: form-data; name=\"password\"" + lineEnd);
        sb.append(lineEnd);
        sb.append("123456");
        String ss = "\r\n--" + boundary + "--\r\n";
        sb.append(ss);
        try {
            ds.write(sb.toString().getBytes("utf-8"),0,sb.toString().getBytes("utf-8").length);// 发送表单字段数据
            ds.flush();
            ds.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(connection.getResponseCode());
        byte[] b = new byte[1024];
        BufferedInputStream s = new BufferedInputStream(connection.getInputStream());
        while(s.read(b)>0){
            System.out.println(new String(b, StandardCharsets.UTF_8));
        }

    }
}
