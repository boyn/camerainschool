package top.boyn.camerainschool;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.camerainschool.controller.UserController;
import top.boyn.camerainschool.mapper.UserMapper;
import top.boyn.camerainschool.service.UserService;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/1
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class UserServiceTest {

    @Resource
    UserService service;

    @Test
    public void getUsers1(){
        System.out.println(service.getAllUsers());
    }

    @Test
    /**
     * 在Test1缓存后,Test2读取的速度有明显提升
     */
    public void getUsers2(){
        System.out.println(service.getAllUsers());
    }

}
